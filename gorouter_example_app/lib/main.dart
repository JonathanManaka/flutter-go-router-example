import 'package:flutter/material.dart';
import 'package:gorouter_example_app/errorScreen.dart';
import 'package:gorouter_example_app/home_screem.dart';
import 'package:url_strategy/url_strategy.dart';
import 'package:go_router/go_router.dart';

void main() {
  //For removing the /#
  setPathUrlStrategy();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => MaterialApp.router(
        routerDelegate: _router.routerDelegate,
        routeInformationParser: _router.routeInformationParser,
        routeInformationProvider: _router.routeInformationProvider,
      );
  //Calling go routor Constuctor
  final GoRouter _router = GoRouter(
    //Handling error
    errorBuilder: (context, state) => ErrorScreen(error: state.error),
    routes: <GoRoute>[
      GoRoute(
        routes: <GoRoute>[
          //Register pages here

          GoRoute(
              path: 'page2',
              builder: (BuildContext context, GoRouterState state) {
                return const HomePage2();
              }),
          GoRoute(
              path: 'page3',
              builder: (BuildContext context, GoRouterState state) {
                return const HomePage3();
              })
        ],
        path: '/',
        builder: (BuildContext context, GoRouterState state) =>
            const HomePage(),
      ),
    ],
  );
}
