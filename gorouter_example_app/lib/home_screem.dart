import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    //To get the route location
    final router = GoRouter.of(context);
    final routerLocationStr = router.location.toString();
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Home Page')),
        body: Center(
          child: Column(children: [
            ElevatedButton(
                onPressed: () {
                  return context.go('/page2');
                },
                child: Text('Go to page 2')),
            ElevatedButton(
                onPressed: () {
                  return context.go('/page3');
                },
                child: Text('Go to page 3')),
          ]),
        ),
      ),
    );
  }
}

class HomePage2 extends StatelessWidget {
  const HomePage2({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Page 2')),
        body: Center(
          child: Column(children: [
            ElevatedButton(
                onPressed: () {
                  return context.go('/');
                },
                child: Text('Go to home page')),
            ElevatedButton(
                onPressed: () {
                  return context.go('/page3');
                },
                child: Text('Got to page 3')),
          ]),
        ),
      ),
    );
  }
}

class HomePage3 extends StatelessWidget {
  const HomePage3({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Page 3')),
        body: Center(
          child: Column(children: [
            ElevatedButton(
                onPressed: () {
                  return context.go('/');
                },
                child: Text('Go to home page')),
            ElevatedButton(
                onPressed: () {
                  return context.go('/page2');
                },
                child: Text('Go to page 2')),
          ]),
        ),
      ),
    );
  }
}
